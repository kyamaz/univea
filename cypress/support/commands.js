const compareSnapshotCommand = require("cypress-visual-regression/dist/command");

compareSnapshotCommand();

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
const LOGIN_POST_OPT = {
  method: "POST",
  url: "http://localhost:4500/login",
  form: true,
  failOnStatusCode: false
};

const VALID_LOG = "toto";
const VALID_PWD = "rerere";
const VALID_CRED = {
  login: VALID_LOG,
  pwd: VALID_PWD
};

Cypress.Commands.add("login", () => {
  //@ts-ignore
  return cy.request({
    ...LOGIN_POST_OPT,
    body: VALID_CRED
  }).then(resp => {
    window.localStorage.setItem('univea_token', resp.body.token)
  });;
});
