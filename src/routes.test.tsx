import * as React from 'react';
import Login from './containers/Login/Login'
import { render } from 'react-testing-library';
describe('Lodable components', () => {
  it('renders login logo', async () => {
    const { getByTestId } = render(<Login />);
    const logo = getByTestId("login-logo")
    expect(logo).toBeDefined()
  })
})