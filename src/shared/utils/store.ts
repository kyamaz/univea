import { store } from "src";

export function storeDispatcher<T>(
  type: string,
  payload: T
): { type: string; payload: T } {
  return store.dispatch({ type, payload });
}
