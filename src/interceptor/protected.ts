import axios from "axios";
import { AppToken } from "../state/state";
import { API } from "@shared/const";
let instance = axios.create({
  baseURL: API,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json;"
  }
});

instance.interceptors.request.use(
  config => {
    const tk: string = !!AppToken.get()
      ? AppToken.get()
      : window.localStorage.getItem("univea_token");
    AppToken.set(tk);
    config.headers.Authorization = `Bearer ${tk}`;
    return config;
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error);
  }
);
// Add a response interceptor
instance.interceptors.response.use(
  response => {
    // Do something with response data
    return response;
  },
  error => {
    // Do something with response error
    console.log(error);
    return Promise.reject(error);
  }
);

export { instance as PROTECTED_API };
