import * as React from "react";
import { createContext, useReducer } from "react";
import { reducer } from "./reducers";
import { INITIAL_STORE_STATE } from "./const";
import { DispatchAction, StoreState, ValidPayload, Store as StoreModel, Reducer, DispatchToStore } from "@model/index";
//https://medium.com/maxime-heckel/rebuilding-redux-with-hooks-and-context-e16b59faf51c

 const Store = createContext({
   state:INITIAL_STORE_STATE,
   dispatch:null
 });

const customMiddleware = (store:StoreModel) => (next:Function) => (action:DispatchAction<ValidPayload>) =>   next(action);

const compose = (...funcs:Array<Function>)=> (x:Function) =>   funcs.reduceRight((composed, f) => f(composed), x)

const createStore = (reducer:Reducer, initialState:StoreState, middlewares:Array<Function>) :{ state:StoreState, dispatch:DispatchToStore<ValidPayload>}=> {
  const [state, dispatch] = useReducer(reducer, initialState);
  if (typeof middlewares !== "undefined") {
    // return middlewares(createStore)(reducer, initialState);
    const middlewareAPI = {
      getState: () => state,
      dispatch: (action:DispatchAction<ValidPayload>) => dispatch(action)
    };
    const chain = middlewares.map(middleware => middleware(middlewareAPI));
    const enhancedDispatch = compose(...chain)(dispatch);
    return { state, dispatch: enhancedDispatch };
  }
  return { state, dispatch };
};
const Provider = ({ children }) :JSX.Element=> {
  const store :{state:StoreState, dispatch:DispatchToStore<ValidPayload>} = createStore(reducer, INITIAL_STORE_STATE, [customMiddleware]);
  return <Store.Provider value={store}>{children}</Store.Provider>;
};

const connect = (
  mapStateToProps =(state:StoreState, props: Object) => {},
  mapDispatchToProps = (dispatch:DispatchToStore<ValidPayload>) => {}
) => (WrappedComponent:React.ComponentType<any>) :React.ComponentType<any>=> {
  return props => {
    const { dispatch, state } = React.useContext(Store);
    return (
      <WrappedComponent
        dispatch={dispatch}
        {...mapStateToProps(state, props)}
        {...mapDispatchToProps(dispatch)}
        {...props}
      />
    );
  };
};
export {Provider, connect, Store}
