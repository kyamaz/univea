import * as React from "react";
import styled from "styled-components";

export const Tabs = styled.ul`
  display: flex;
  justify-content: space-evenly;
`;
export const TabItem = styled.ul`
  border-bottom: 1px solid #dadee4;
  display: flex;
  flex: 1;
  justify-content: center;
`;
