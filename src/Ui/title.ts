import styled from "styled-components";

export const AppTitle = styled.h1.attrs({ className: "text-center h2" })`
  color: white;
  margin: 30px 0;
  font-weight: 500;
`;
