export class LocalStorageMock {
  private store = {
    univea_token: "someKey"
  };

  clear() {
    this.store = null;
  }

  getItem(key: string) {
    return this.store[key] || null;
  }

  setItem(key: string, value: string) {
    this.store[key] = value;
  }

  removeItem(key: string) {
    delete this.store[key];
  }
}
