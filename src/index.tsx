import * as React from "react";
import { hydrate, render } from "react-dom";
import registerServiceWorker from "./registerServiceWorker";
//css library

import "./styles.css";
//style
import { GlobalStyle } from "@shared/style";

import  "react-grid-layout/css/styles.css"
import "react-resizable/css/styles.css"

import 'grapesjs/dist/css/grapes.min.css';

//navigation
import { Route, Switch, Router } from "react-router-dom";
import { history } from "./route-history";

//routes
import { AppContainer, LoginContainer, PageBuilderContainer, OryBuilderContainer, GrapeJsContainer } from "./routes";
import ProtectedRoute from "./containers/ProtectedRoute/ProtectedRoute";
//translation
import { IntlProvider, addLocaleData } from "react-intl";
import { useLocalLang } from "@hooks/useLocalLang";
import configureStore from "@store/config.store";
//use key for dinamic language selection https://github.com/yahoo/react-intl/wiki/Components

//redux
import { Provider, ReactRedux, connect } from "react-redux";
import { SET_APP_LANG } from "@store/app";
import { storeDispatcher } from "@shared/utils/store";

//interface
import { AppLang } from "@model/app";

export const store = configureStore();

 let AppWrap:(props)=> JSX.Element  =(props) =>{
  const browserLang = navigator.language.split(/[-_]/)[0]; // language without region code
  const [locale, localData] = useLocalLang(browserLang);
  storeDispatcher<AppLang>(SET_APP_LANG, locale )
  return (
    <Provider store={store}>
      <IntlProvider locale={locale} key={locale} messages={localData}>
        <Router history={history}>
            <GlobalStyle />
            <Switch>
              <Route
                path="/login"
                render={props => <LoginContainer {...props} />}
              />
              <ProtectedRoute path="/page-builder" exact  component={PageBuilderContainer} />
              <ProtectedRoute path="/ory-builder" exact  component={OryBuilderContainer} />
              <ProtectedRoute path="/grape-builder" exact  component={GrapeJsContainer} />

      
              
              <ProtectedRoute path="/" exact component={AppContainer} />
              <ProtectedRoute component={AppContainer} />

            </Switch>
        </Router>
      </IntlProvider>
    </Provider>
  );
}

//websjonp error temp  workaround  
//render(<AppWrap />, document.getElementById("root") as HTMLElement);
  const rootElement = document.getElementById("root");
 if (rootElement.hasChildNodes()) {
  hydrate(<AppWrap />, rootElement);
} else {
  render(<AppWrap />, rootElement);
}  
 
registerServiceWorker();
