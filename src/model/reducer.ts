import { AppLang } from "@model/app";
export interface AppReducer {
  lang: AppLang;
}

export interface ListTypeReducer<T> {
  list: Array<T>;
  loading: boolean;
  error: boolean;
}
