import * as React from "react";

import { NavHeader, NavSection, NavLinkItem } from "@ui/navbar";

function appNav(props: any): JSX.Element {
  return (
    <NavHeader>
      <NavSection data-testid="links">
        <NavLinkItem exact={true} to="/" data-testid="link-home">
          home
        </NavLinkItem>
        <NavLinkItem exact={true} to="/page-builder" data-testid="link-page">
          page builder
        </NavLinkItem>
        <NavLinkItem exact={true} to="/ory-builder" data-testid="link-ory">
          ory 
        </NavLinkItem>
        <NavLinkItem exact={true} to="/grape-builder" data-testid="grape-ory">
          grapejs 
        </NavLinkItem>
      </NavSection>
    </NavHeader>
  );
}

export { appNav as AppNav };
