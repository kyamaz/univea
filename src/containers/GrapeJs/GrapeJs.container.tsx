import * as React from "react";
import { Component, Fragment } from "react";
import grapesjs from "grapesjs";
import gjsPresetWebpage from "grapesjs-preset-webpage";
import { PrimaryBtn } from "@ui/btn";

class GrapeJs extends Component {
  el;
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.el = grapesjs.init({
      container: "#gjs",
      components: "<h1>Hello World Component!</h1>",
      fromElement: true,
      // Size of the editor
      height: '100vh',
      width: 'auto',
      // Disable the storage manager for the moment
      storageManager: { type: null },
      // Avoid any default panel
      panels: { defaults: [] },

      blockManager: {
        appendTo: "#blocks",
        blocks: [
          {
            id: "section", // id is mandatory
            label: "<b>Section</b>", // You can use HTML/SVG inside labels
            attributes: { class: "gjs-block-section" },
            content: `<section>
                      <h1>This is a simple title</h1>
                      <div>This is just a Lorem text: Lorem ipsum dolor sit amet</div>
                    </section>`
          },
          {
            id: "text",
            label: "Text",
            content: '<div data-gjs-type="text">Insert your text here</div>'
          },
          {
            id: "image",
            label: "Image",
            // Select the component once it's dropped
            select: true,
            // You can pass components as a JSON instead of a simple HTML string,
            // in this case we also use a defined component type `image`
            content: { type: "image" },
            // This triggers `active` event on dropped components and the `image`
            // reacts by opening the AssetManager
            activate: true
          }
        ]
      },
  /*     plugins: [gjsPresetWebpage],
      pluginsOpts: {
        "gjs-preset-webpage": {
          // options
        }
      } */

    });
  }
  render() {
    return (
      <div style={{display:"flex", flex:1, backgroundColor:"orange" }}>
        <div style={{flex:0.8}} id="gjs" ref={el => (this.el = el)} />

        <div style={{flex:0.2}}  id="blocks" />
      </div>
    );
  }
}

export default GrapeJs;
