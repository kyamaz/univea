import * as React from "react";
import { Fragment } from "react";

import { Helmet } from "react-helmet";
import { APP_NAME } from "@shared/const";
import {connect} from 'react-redux'
import { AppReducer } from "@model/index";
interface PageWrapProps {
  description?:string;
  children: JSX.Element;
  AppReducer?:AppReducer
}

//https://github.com/nfl/react-helmet/issues/437
//use beta for now
let pageWrap=(props: PageWrapProps) =>{
  const { description, AppReducer:{lang}}=props
  return (
    <Fragment>
      <Helmet
        htmlAttributes={{ lang }}
        defaultTitle={`${APP_NAME}`}
        titleTemplate={`${APP_NAME}`}
      >
        <title itemProp="name" lang="en">
          {`${APP_NAME}`}
        </title>
        <meta name="description" content={description} />
      </Helmet>
      {props.children}
    </Fragment>
  );
}

pageWrap=connect(
({AppReducer})=>({AppReducer}),
null)(pageWrap)

export { pageWrap as PageWrap };
