import * as React from "react";
import { AppTitle } from "@ui/title";
import { fetchList } from "@store/tv";
import { connect } from "react-redux";
import { PageWrap } from "@containers/PageWrap/PageWrap";
import { Fragment } from "react";
interface AppComponentProps {
  fetchList?: Function;
  TvReducer?: any;
}
const description = "mind blowing";

let App = (props: AppComponentProps) => {
  React.useEffect(() => {
    props.fetchList();
  }, []);
  const {
    TvReducer: { list }
  } = props;

  return (
    <div>
      <PageWrap description={description}>
        <Fragment>
          <AppTitle data-testid="app-title">
            redux observable with hooks
          </AppTitle>
          <ul>
            {list.map(el => (
              <li key={el.id}>{el.name}</li>
            ))}
          </ul>
        </Fragment>
      </PageWrap>
    </div>
  );
};

App = connect(
  ({ TvReducer }) => ({ TvReducer }),
  { fetchList }
)(App);

export default App;
