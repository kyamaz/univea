import * as React from 'react';
import App from './App';
import { cleanup, render } from 'react-testing-library';
import { testIdGetter } from '@mock/mock.utils';

describe("Access to app", () => {
  // automatically unmount and cleanup DOM after the test is finished.
  afterEach(cleanup);
  test("It should renders the app title", () => {
    const { getByTestId } = render(<App />);
    const { appTitle } = testIdGetter(
      ["appTitle", "app-title"],
      getByTestId
    );
      expect(appTitle.textContent).toBe('Stateless simple react hook starter')
  });

})