import *  as  React from 'react'
import {Component} from 'react'

// The editor core
import Editor, { Editable, createEmptyState } from 'ory-editor-core'
import 'ory-editor-core/lib/index.css' // we also want to load the stylesheets

// Require our ui components (optional). You can implement and use your own ui too!
import { Trash, DisplayModeToggle, Toolbar } from 'ory-editor-ui'
import 'ory-editor-ui/lib/index.css'

// Load some exemplary plugins:
import slate from 'ory-editor-plugins-slate' // The rich text area plugin
import 'ory-editor-plugins-slate/lib/index.css' // Stylesheets for the rich text area plugin


//require('react-tap-event-plugin')() // react-tap-event-plugin is required by material-ui which is used by ory-editor-ui so we need to call it here
//Define which plugins we want to use. We only have slate and parallax available, so load those.

import image from 'ory-editor-plugins-image'
import 'ory-editor-plugins-image/lib/index.css'

const plugins = {
  content: [slate()], // Define plugins for content cells. To import multiple plugins, use [slate(), image, spacer, divider]
}

// Creates an empty editable
const content = createEmptyState()

// Instantiate the editor
const editor = new Editor({
  plugins,
  // pass the content state - you can add multiple editables here
  editables: [content],
})

//editor.editable.update("couou") 

class OryBuilder extends Component<any, any> {
  constructor(props){
    super(props)
    this.state = {
      content: {}
    };
  }
  componentDidMount() {
    const { content } = this.props;
   // editor.trigger.editable.update(content);
  }

  onEditorStateChange = state => {
    this.setState({ content: state },()=>console.log(this.state));
   
  };


  render() {
    return (
      <div>
        <div className="App-header">
          <h2>Orry page builder</h2>
        </div>

        {/* Content area */}
         <Editable editor={editor} id={content.id} onChange={this.onEditorStateChange} />
 
        {/*  Default user interface  */}
        <Trash editor={editor}/>
        <DisplayModeToggle editor={editor}/>
        <Toolbar editor={editor}/> 
      </div>
    );
  }
}


export default OryBuilder