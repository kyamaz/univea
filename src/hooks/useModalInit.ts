import { useEffect } from "react";
import * as Modal from "react-modal";

export function useModalInit(el: any): void {
  useEffect(() => {
    Modal.setAppElement(el.current);
  }, []);
}
