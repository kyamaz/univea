import { AppLang } from "./../model/app";
import * as React from "react";
import { addLocaleData } from "react-intl";
import en from "react-intl/locale-data/en";
import de from "react-intl/locale-data/de";
import localData from "../assets/locales";
import { UseHooks } from "@model/index";

addLocaleData([...en, ...de]);
const FALLBACK_EN: AppLang = "de";

export function useLocalLang(
  lang: string = FALLBACK_EN
): [AppLang, { [key: string]: string }] {
  const [useLang, setUseLang]: UseHooks<AppLang> = React.useState(FALLBACK_EN);
  React.useEffect(() => {
    const locale = localData.hasOwnProperty(lang) ? lang : FALLBACK_EN;
    setUseLang(<AppLang>locale);
  }, [lang]);
  return [useLang, localData[useLang]];
}
