import * as React from "react";
export function useLatestValue<T>(value: T, initialValue: T): [T] {
  const [updatedValue, setUpdatedValue] = React.useState(initialValue);
  React.useEffect(() => {
    setUpdatedValue(value);
  }, [value]);
  return [updatedValue];
}
