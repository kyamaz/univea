import { fetchListEpic } from "./tv/tv.epics";
import { combineEpics } from "redux-observable";
import { combineReducers } from "redux";
import { TvReducer } from "./tv/tv.reducer";
import { AppReducer } from "./app/app.reducer";
export const rootEpic = combineEpics(fetchListEpic);

export const rootReducer = combineReducers({
  TvReducer,
  AppReducer
});
