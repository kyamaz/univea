import { FETCH_LIST_TRY, FETCH_LIST_SUCCESS } from "@store/tv";

export const fetchList = () => ({ type: FETCH_LIST_TRY });
export const fetchListSuccess = payload => ({
  type: FETCH_LIST_SUCCESS,
  payload
});
