import { TV_ENDPOINT } from "@shared/const";
import { fetchListSuccess } from "./tv.actions";
import { ofType } from "redux-observable";
import { FETCH_LIST_TRY } from "@store/tv";
import { mergeMap, map } from "rxjs/operators";
import { PROTECTED_API } from "@interceptor/protected";
import { from } from "rxjs";
// epic
export const fetchListEpic = action$ =>
  action$.pipe(
    ofType(FETCH_LIST_TRY),
    mergeMap(action =>
      from(PROTECTED_API.get(TV_ENDPOINT)).pipe(
        map(response => fetchListSuccess(response.data.data))
      )
    )
  );
