import { DispatchAction } from "@model/index";
import { ListTypeReducer } from "@model/reducer";
import { FETCH_LIST_SUCCESS, FETCH_LIST_TRY } from "@store/tv";
const initialUsersState: ListTypeReducer<any> = {
  list: [],
  loading: false,
  error: false
};

function tvReducer(
  state = initialUsersState,
  action: DispatchAction<any>
): ListTypeReducer<any> {
  switch (action.type) {
    case FETCH_LIST_TRY: {
      return {
        ...state,
        loading: true
      };
    }
    case FETCH_LIST_SUCCESS: {
      return {
        ...state,
        list: action.payload,
        loading: false
      };
    }

    default:
      return state;
  }
}

export { tvReducer as TvReducer };
