import { DispatchAction } from "./../../model/store";
import { AppReducer } from "./../../model/reducer";
import { SET_APP_LANG } from "./app.actionTypes";

const initialAppState: AppReducer = {
  lang: null
};

function appReducer(
  state = initialAppState,
  action: DispatchAction<any>
): AppReducer {
  const { payload } = action;
  switch (action.type) {
    case SET_APP_LANG: {
      return {
        ...state,
        lang: payload
      };
    }
    default:
      return state;
  }
}

export { appReducer as AppReducer };
