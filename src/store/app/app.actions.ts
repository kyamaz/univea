import { SET_APP_LANG } from "./app.actionTypes";
export const setAppLang = (payload: "en" | "de") => ({
  type: SET_APP_LANG,
  payload
});
